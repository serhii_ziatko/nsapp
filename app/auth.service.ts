import { Injectable } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router'

@Injectable()
export class AuthService {
  auth: boolean = false;
  constructor(
    private routerExt: RouterExtensions,
  ) { }

  setAuth(): void {
    this.auth = true;
    localStorage.setItem('login', 'true');
  }

  removeAuth(): void {
    this.auth = false;
    localStorage.removeItem('login');
  }

  isAuth(): boolean {
    if (this.auth) {
      return true;
    }
    if (localStorage.getItem('login')) {
      return this.auth = true;
    }
    return false;
  }
}