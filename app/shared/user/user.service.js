"use strict";
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
require('rxjs/add/operator/do');
require('rxjs/add/operator/map');
var config_1 = require('../config');
var auth_service_1 = require('../../auth.service');
var UserService = (function () {
    function UserService(http, authService) {
        this.http = http;
        this.authService = authService;
    }
    UserService.prototype.login = function (user) {
        var _this = this;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(config_1.Config.apiUrl + '/users', JSON.stringify({
            email: user.email,
            password: user.password
        }), { headers: headers }).map(function (response) { return response.json(); })
            .do(function (user) {
            _this.authService.setAuth();
        }).catch(this.handleErrors);
    };
    UserService.prototype.singup = function (user) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(config_1.Config.apiUrl + '/users/singup', JSON.stringify({
            email: user.email,
            password: user.password
        }), { headers: headers }).map(function (response) { return response.json(); })
            .catch(this.handleErrors);
    };
    UserService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error.json()));
        return Rx_1.Observable.throw(error);
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, auth_service_1.AuthService])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
;
//# sourceMappingURL=user.service.js.map