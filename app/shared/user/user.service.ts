import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { User } from './user';
import { Config } from '../config';
import { AuthService } from '../../auth.service'

@Injectable()
export class UserService {
  constructor(
    private http: Http,
    private authService: AuthService,
  ) { }

  login(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      Config.apiUrl + '/users',
      JSON.stringify({
        email: user.email,
        password: user.password
      }),
      { headers: headers }
    ).map(response => response.json())
      .do((user) => {
        this.authService.setAuth();
      }).catch(this.handleErrors);
  }

  singup(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      Config.apiUrl + '/users/singup',
      JSON.stringify({
        email: user.email,
        password: user.password
      }),
      { headers: headers }
    ).map(response => response.json())
    .catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }
};
