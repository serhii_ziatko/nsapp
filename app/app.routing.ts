import { LoginComponent } from './pages/login/login.component';
import { AppComponent } from './app.component';
import { MenuComponent } from './pages/menu/menu.component';


export const routes = [
  { path: '', component: AppComponent },
  { path: 'login', component: LoginComponent },
  { path: 'menu', component: MenuComponent },
];

export const navigatableComponents = [
  LoginComponent,
  MenuComponent,
];