"use strict";
var core_1 = require('@angular/core');
var platform_1 = require('nativescript-angular/platform');
var forms_1 = require('nativescript-angular/forms');
var nativescript_angular_1 = require('nativescript-angular');
require('nativescript-localstorage');
var router_1 = require('nativescript-angular/router');
var app_routing_1 = require('./app.routing');
// import { SideMenuComponent } from './components/sidemenu/sidemenu.component'
var auth_service_1 = require('./auth.service');
var user_service_1 = require('./shared/user/user.service');
var app_component_1 = require('./app.component');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent
            ].concat(app_routing_1.navigatableComponents),
            bootstrap: [app_component_1.AppComponent],
            imports: [
                platform_1.NativeScriptModule,
                router_1.NativeScriptRouterModule,
                forms_1.NativeScriptFormsModule,
                nativescript_angular_1.NativeScriptHttpModule,
                router_1.NativeScriptRouterModule.forRoot(app_routing_1.routes),
            ],
            schemas: [core_1.NO_ERRORS_SCHEMA],
            // exports: [SideMenuComponent],
            providers: [auth_service_1.AuthService, user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map