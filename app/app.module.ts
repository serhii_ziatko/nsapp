import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/platform';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpModule } from 'nativescript-angular';
import 'nativescript-localstorage';
import { NativeScriptRouterModule } from 'nativescript-angular/router';


import { routes, navigatableComponents } from './app.routing'
// import { SideMenuComponent } from './components/sidemenu/sidemenu.component'
import { AuthService } from './auth.service'
import { UserService } from './shared/user/user.service'

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    ...navigatableComponents
  ],
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    NativeScriptRouterModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NativeScriptRouterModule.forRoot(routes),
  ],
  schemas: [NO_ERRORS_SCHEMA],
  // exports: [SideMenuComponent],
  providers: [AuthService, UserService]
})
export class AppModule { }
