import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from 'ui/page';
import { View } from 'ui/core/view';
import { TextField } from 'ui/text-field';
import { Color } from 'color';
import { Router } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router'

import { User } from '../../shared/user/user.js';
import { UserService } from '../../shared/user/user.service'
import { setHintColor } from '../../utils/hint-util';
import { AuthService } from '../../auth.service'


@Component({
  selector: 'login',
  templateUrl: 'pages/login/login.html',
  styleUrls: ['pages/login/login-common.css', 'pages/login/login-android.css'],
})

export class LoginComponent implements OnInit {
  @ViewChild('cover') cover: ElementRef;
  @ViewChild('email') email: ElementRef;
  @ViewChild('password') password: ElementRef;
  user: User = new User();
  isLoggingIn: boolean = true;
  background: any;
  showLoading: boolean = false;

  constructor(
    private page: Page,
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private routerExt: RouterExtensions
  ) {}
  ngOnInit() {
    this.user.email = 'ziatko@mail.ru';
    this.user.password = '563289';
    this.page.actionBarHidden = true;
    this.changeBackground();
  }

  login(): void {
    this.userService.login(this.user)
      .subscribe((user) => {
        this.routerExt.navigate(['/menu'], {
          clearHistory: true,
          transition: {
            name: 'flip',
            duration: 500,
            curve: 'linear'
          }
        });
        this.isLoggingIn = true;
        this.showLoading = false;
      }, (err) => {
        alert('Unfortunately we could not find your account.')
        this.showLoading = false;
      });
  }

  singup(): void {
    this.userService.singup(this.user)
      .subscribe((user) => {
        alert('Sing up success!')
        this.toggleDisplay();
        this.showLoading = false;
      }, (err) => {
        alert('Unfortunately we could not create your account. Such email already is exist!')
        this.showLoading = false;
      })
  }

  submit(): void {
    this.showLoading = true;
    if (!this.user.isValidEmail()) {
      alert('Email is not valid');
      return;
    }
    if (this.isLoggingIn) {
      this.login();
    } else {
      this.singup();
    }
  }
  toggleDisplay(): void {
    this.isLoggingIn = !this.isLoggingIn;
    this.animateBackground();
  }

  animateBackground(): void {
    let cover = <View>this.cover.nativeElement;
    cover.animate({ opacity: 0.8, duration: 200 })
      .then(() => {
        this.changeBackground();
        this.setTextFieldColors();
        cover.animate({ opacity: 0, duration: 200 })
      })
  }
  changeBackground(): void {
    this.background = {'background-image': this.isLoggingIn ? 'url(\'res://background_logo\')' : 'url(\'res://background\')'}
  }

  setTextFieldColors() {
    let emailTextField = <TextField>this.email.nativeElement;
    let passwordTextField = <TextField>this.password.nativeElement;

    let mainTextColor = new Color(this.isLoggingIn ? 'black' : 'white');
    emailTextField.color = mainTextColor;
    passwordTextField.color = mainTextColor;

    let hintColor = new Color(this.isLoggingIn ? '#448AFF' : '#90CAF9');
    setHintColor({ view: emailTextField, color: hintColor });
    setHintColor({ view: passwordTextField, color: hintColor });
  }



}