"use strict";
var core_1 = require('@angular/core');
var page_1 = require('ui/page');
var color_1 = require('color');
var router_1 = require('@angular/router');
var router_2 = require('nativescript-angular/router');
var user_js_1 = require('../../shared/user/user.js');
var user_service_1 = require('../../shared/user/user.service');
var hint_util_1 = require('../../utils/hint-util');
var auth_service_1 = require('../../auth.service');
var LoginComponent = (function () {
    function LoginComponent(page, userService, authService, router, routerExt) {
        this.page = page;
        this.userService = userService;
        this.authService = authService;
        this.router = router;
        this.routerExt = routerExt;
        this.user = new user_js_1.User();
        this.isLoggingIn = true;
        this.showLoading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.user.email = 'ziatko@mail.ru';
        this.user.password = '563289';
        this.page.actionBarHidden = true;
        this.changeBackground();
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.userService.login(this.user)
            .subscribe(function (user) {
            _this.routerExt.navigate(['/menu'], {
                clearHistory: true,
                transition: {
                    name: 'flip',
                    duration: 500,
                    curve: 'linear'
                }
            });
            _this.isLoggingIn = true;
            _this.showLoading = false;
        }, function (err) {
            alert('Unfortunately we could not find your account.');
            _this.showLoading = false;
        });
    };
    LoginComponent.prototype.singup = function () {
        var _this = this;
        this.userService.singup(this.user)
            .subscribe(function (user) {
            alert('Sing up success!');
            _this.toggleDisplay();
            _this.showLoading = false;
        }, function (err) {
            alert('Unfortunately we could not create your account. Such email already is exist!');
            _this.showLoading = false;
        });
    };
    LoginComponent.prototype.submit = function () {
        this.showLoading = true;
        if (!this.user.isValidEmail()) {
            alert('Email is not valid');
            return;
        }
        if (this.isLoggingIn) {
            this.login();
        }
        else {
            this.singup();
        }
    };
    LoginComponent.prototype.toggleDisplay = function () {
        this.isLoggingIn = !this.isLoggingIn;
        this.animateBackground();
    };
    LoginComponent.prototype.animateBackground = function () {
        var _this = this;
        var cover = this.cover.nativeElement;
        cover.animate({ opacity: 0.8, duration: 200 })
            .then(function () {
            _this.changeBackground();
            _this.setTextFieldColors();
            cover.animate({ opacity: 0, duration: 200 });
        });
    };
    LoginComponent.prototype.changeBackground = function () {
        this.background = { 'background-image': this.isLoggingIn ? 'url(\'res://background_logo\')' : 'url(\'res://background\')' };
    };
    LoginComponent.prototype.setTextFieldColors = function () {
        var emailTextField = this.email.nativeElement;
        var passwordTextField = this.password.nativeElement;
        var mainTextColor = new color_1.Color(this.isLoggingIn ? 'black' : 'white');
        emailTextField.color = mainTextColor;
        passwordTextField.color = mainTextColor;
        var hintColor = new color_1.Color(this.isLoggingIn ? '#448AFF' : '#90CAF9');
        hint_util_1.setHintColor({ view: emailTextField, color: hintColor });
        hint_util_1.setHintColor({ view: passwordTextField, color: hintColor });
    };
    __decorate([
        core_1.ViewChild('cover'), 
        __metadata('design:type', core_1.ElementRef)
    ], LoginComponent.prototype, "cover", void 0);
    __decorate([
        core_1.ViewChild('email'), 
        __metadata('design:type', core_1.ElementRef)
    ], LoginComponent.prototype, "email", void 0);
    __decorate([
        core_1.ViewChild('password'), 
        __metadata('design:type', core_1.ElementRef)
    ], LoginComponent.prototype, "password", void 0);
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: 'pages/login/login.html',
            styleUrls: ['pages/login/login-common.css', 'pages/login/login-android.css'],
        }), 
        __metadata('design:paramtypes', [page_1.Page, user_service_1.UserService, auth_service_1.AuthService, router_1.Router, router_2.RouterExtensions])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map