"use strict";
var core_1 = require('@angular/core');
var animationModule = require('ui/animation');
var dialogs = require('ui/dialogs');
var router_1 = require('nativescript-angular/router');
var user_service_1 = require('../../shared/user/user.service');
var auth_service_1 = require('../../auth.service');
var MenuComponent = (function () {
    function MenuComponent(authService, routerExt) {
        this.authService = authService;
        this.routerExt = routerExt;
        this.menuIsShow = false;
        this.source = [
            { title: 'Main' },
            { title: 'History' },
            { title: 'New Results' },
        ];
    }
    ;
    MenuComponent.prototype.ngOnInit = function () { };
    MenuComponent.prototype.goToLoginPage = function () {
        var _this = this;
        dialogs.confirm({
            title: "Are you sure&",
            message: "You want logout!!",
            okButtonText: "Ok",
            cancelButtonText: "Cancel",
        }).then(function (result) {
            if (result) {
                _this.authService.removeAuth();
                _this.routerExt.navigate(['/login'], {
                    clearHistory: true,
                    transition: {
                        name: "flipLeft",
                        duration: 500,
                        curve: "linear"
                    }
                });
            }
        });
    };
    MenuComponent.prototype.showMenu = function () {
        var menuArea = this.menuArea.nativeElement;
        var menuBackground = this.menuBackground.nativeElement;
        this.menuIsShow = true;
        var definitions = new Array();
        definitions.push({ target: menuBackground, opacity: 1, duration: 100 });
        definitions.push({ target: menuArea, translate: { x: 0, y: 0 }, duration: 300 });
        var playSequentially = true;
        var animationSet = new animationModule.Animation(definitions, playSequentially);
        animationSet.play()
            .catch(function (e) {
            console.log(e.message);
        });
    };
    MenuComponent.prototype.hideMenu = function () {
        var _this = this;
        var menuArea = this.menuArea.nativeElement;
        var menuBackground = this.menuBackground.nativeElement;
        var definitions = new Array();
        definitions.push({ target: menuArea, translate: { x: -300, y: 0 }, duration: 300 });
        definitions.push({ target: menuBackground, opacity: 0, duration: 100 });
        var playSequentially = true;
        var animationSet = new animationModule.Animation(definitions, playSequentially);
        animationSet.play().then(function () {
            _this.menuIsShow = false;
        })
            .catch(function (e) {
            console.log(e.message);
        });
    };
    MenuComponent.prototype.toggleMenu = function () {
        if (this.menuIsShow) {
            this.hideMenu();
            return;
        }
        this.showMenu();
    };
    MenuComponent.prototype.swipeToMenu = function (e) {
        if (e.direction == 1) {
            this.showMenu();
        }
        if (e.direction == 2) {
            this.hideMenu();
        }
    };
    __decorate([
        core_1.ViewChild('menuArea'), 
        __metadata('design:type', core_1.ElementRef)
    ], MenuComponent.prototype, "menuArea", void 0);
    __decorate([
        core_1.ViewChild('menuBackground'), 
        __metadata('design:type', core_1.ElementRef)
    ], MenuComponent.prototype, "menuBackground", void 0);
    MenuComponent = __decorate([
        core_1.Component({
            selector: 'menu',
            providers: [user_service_1.UserService],
            templateUrl: 'pages/menu/menu.html',
            styleUrls: ['pages/menu/menu-common.css', 'pages/menu/menu-android.css'],
        }), 
        __metadata('design:paramtypes', [auth_service_1.AuthService, router_1.RouterExtensions])
    ], MenuComponent);
    return MenuComponent;
}());
exports.MenuComponent = MenuComponent;
;
//# sourceMappingURL=menu.component.js.map