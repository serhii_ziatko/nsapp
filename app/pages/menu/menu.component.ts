import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { Page } from 'ui/page';
import { View } from 'ui/core/view';
import * as enums from 'ui/enums';
import * as animationModule from 'ui/animation'
import * as dialogs  from 'ui/dialogs';
import { RouterExtensions } from 'nativescript-angular/router'

import { User } from '../../shared/user/user.js';
import { UserService } from '../../shared/user/user.service'
import { AuthService } from '../../auth.service'


@Component({
  selector: 'menu',
  providers: [UserService],
  templateUrl: 'pages/menu/menu.html',
  styleUrls: ['pages/menu/menu-common.css', 'pages/menu/menu-android.css'],
})

export class MenuComponent implements OnInit {
  @ViewChild('menuArea') menuArea: ElementRef;
  @ViewChild('menuBackground') menuBackground: ElementRef;
  menuIsShow: boolean = false;

  source = [
    { title:'Main' },
    { title: 'History' },
    { title: 'New Results' },
  ]

  constructor(
    private authService: AuthService,
    private routerExt: RouterExtensions,
  ) { };

  ngOnInit(){}

  goToLoginPage(): void {
    dialogs.confirm({
      title: "Are you sure&",
      message: "You want logout!!",
      okButtonText: "Ok",
      cancelButtonText: "Cancel",
    }).then((result) => {
      if (result) {
        this.authService.removeAuth();
        this.routerExt.navigate(['/login'], {
          clearHistory: true,
          transition: {
              name: "flipLeft",
              duration: 500,
              curve: "linear"
          }
        })
      }
    });
  }

  showMenu(): void {
    let menuArea = <View>this.menuArea.nativeElement;
    let menuBackground = <View>this.menuBackground.nativeElement;

    this.menuIsShow = true;

    const definitions = new Array<animationModule.AnimationDefinition>();
    definitions.push({target: menuBackground, opacity: 1, duration: 100 });
    definitions.push({target: menuArea, translate: {x: 0, y: 0}, duration: 300 });
    const playSequentially = true;
    const animationSet = new animationModule.Animation(definitions, playSequentially);
    animationSet.play()
    .catch((e) => {
        console.log(e.message);
    });

  }
  hideMenu(): void {
    let menuArea = <View>this.menuArea.nativeElement;
    let menuBackground = <View>this.menuBackground.nativeElement;

    const definitions = new Array<animationModule.AnimationDefinition>();
    definitions.push({target: menuArea, translate: {x: -300, y: 0}, duration: 300 });
    definitions.push({target: menuBackground, opacity: 0, duration: 100 });
    const playSequentially = true;
    const animationSet = new animationModule.Animation(definitions, playSequentially);
    animationSet.play().then(() => {
        this.menuIsShow = false;
    })
    .catch((e) => {
        console.log(e.message);
    });
  }

  toggleMenu() {
    if (this.menuIsShow) {
      this.hideMenu();
      return;
    }
    this.showMenu();
  }
  swipeToMenu(e) {
    if (e.direction == 1) {
      this.showMenu();
    }
    if (e.direction == 2) {
      this.hideMenu();
    }
  }

};