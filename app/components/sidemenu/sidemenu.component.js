// import { Component, ViewChild, ElementRef } from '@angular/core';
// import { View } from 'ui/core/view';
// import * as enums from 'ui/enums';
// import * as animationModule from 'ui/animation'
// import * as dialogs  from 'ui/dialogs';
// @Component({
//   selector: 'side-menu',
//   templateUrl: 'components/sidemenu/sidemenu.html',
//   styleUrls: ['components/sidemenu/sidemenu-common.css', 'components/sidemenu/sidemenu-android.css'],
// })
// export class SideMenuComponent {
//   @ViewChild('menuArea') menuArea: ElementRef;
//   @ViewChild('menuBackground') menuBackground: ElementRef;
//   menuIsShow: boolean = false;
//   constructor() {}
//   source = [
//     { title:'Main' },
//     { title: 'History' },
//     { title: 'New Results' },
//   ]
//   showMenu(): void {
//     let menuArea = <View>this.menuArea.nativeElement;
//     let menuBackground = <View>this.menuBackground.nativeElement;
//     this.menuIsShow = true;
//     const definitions = new Array<animationModule.AnimationDefinition>();
//     definitions.push({target: menuBackground, opacity: 1, duration: 100 });
//     definitions.push({target: menuArea, translate: {x: 0, y: 0}, duration: 300 });
//     const playSequentially = true;
//     const animationSet = new animationModule.Animation(definitions, playSequentially);
//     animationSet.play()
//     .catch((e) => {
//         console.log(e.message);
//     });
//   }
//   hideMenu(): void {
//     let menuArea = <View>this.menuArea.nativeElement;
//     let menuBackground = <View>this.menuBackground.nativeElement;
//     const definitions = new Array<animationModule.AnimationDefinition>();
//     definitions.push({target: menuArea, translate: {x: -300, y: 0}, duration: 300 });
//     definitions.push({target: menuBackground, opacity: 0, duration: 100 });
//     const playSequentially = true;
//     const animationSet = new animationModule.Animation(definitions, playSequentially);
//     animationSet.play().then(() => {
//         this.menuIsShow = false;
//     })
//     .catch((e) => {
//         console.log(e.message);
//     });
//   }
//   toggleMenu() {
//     if (this.menuIsShow) {
//       this.hideMenu();
//       return;
//     }
//     this.showMenu();
//   }
//   swipeToMenu(e) {
//     if (e.direction == 1) {
//       this.showMenu();
//     }
//     if (e.direction == 2) {
//       this.hideMenu();
//     }
//   }
// } 
//# sourceMappingURL=sidemenu.component.js.map