"use strict";
var login_component_1 = require('./pages/login/login.component');
var app_component_1 = require('./app.component');
var menu_component_1 = require('./pages/menu/menu.component');
exports.routes = [
    { path: '', component: app_component_1.AppComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'menu', component: menu_component_1.MenuComponent },
];
exports.navigatableComponents = [
    login_component_1.LoginComponent,
    menu_component_1.MenuComponent,
];
//# sourceMappingURL=app.routing.js.map