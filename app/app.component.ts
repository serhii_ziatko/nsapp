import { Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router'

import { AuthService } from './auth.service'

@Component({
  selector: 'my-app',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private routerExt: RouterExtensions,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.route(this.authService.isAuth());
  }

  route(isAuth: boolean): void {
    if (isAuth) {
      this.routerExt.navigate(['/menu'], {
        clearHistory: true,
        transition: {
          name: "flip",
          duration: 1000,
          curve: "linear"
        }
      })
    } else {
      this.routerExt.navigate(['/login'], {
        clearHistory: true,
        transition: {
            name: "flip",
            duration: 1000,
            curve: "linear"
        }
      })
    }
  }

}
