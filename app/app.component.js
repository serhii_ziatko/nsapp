"use strict";
var core_1 = require('@angular/core');
var router_1 = require('nativescript-angular/router');
var auth_service_1 = require('./auth.service');
var AppComponent = (function () {
    function AppComponent(routerExt, authService) {
        this.routerExt = routerExt;
        this.authService = authService;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.route(this.authService.isAuth());
    };
    AppComponent.prototype.route = function (isAuth) {
        if (isAuth) {
            this.routerExt.navigate(['/menu'], {
                clearHistory: true,
                transition: {
                    name: "flip",
                    duration: 1000,
                    curve: "linear"
                }
            });
        }
        else {
            this.routerExt.navigate(['/login'], {
                clearHistory: true,
                transition: {
                    name: "flip",
                    duration: 1000,
                    curve: "linear"
                }
            });
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.RouterExtensions, auth_service_1.AuthService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map