"use strict";
var core_1 = require('@angular/core');
var router_1 = require('nativescript-angular/router');
var AuthService = (function () {
    function AuthService(routerExt) {
        this.routerExt = routerExt;
        this.auth = false;
    }
    AuthService.prototype.setAuth = function () {
        this.auth = true;
        localStorage.setItem('login', 'true');
    };
    AuthService.prototype.removeAuth = function () {
        this.auth = false;
        localStorage.removeItem('login');
    };
    AuthService.prototype.isAuth = function () {
        if (this.auth) {
            return true;
        }
        if (localStorage.getItem('login')) {
            return this.auth = true;
        }
        return false;
    };
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_1.RouterExtensions])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map